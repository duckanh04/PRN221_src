using LoadDB_EF_RazorPage.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LoadDB_EF_RazorPage.Pages.Students
{
    public class Delete_StudentModel : PageModel
    {
		private readonly PRN211_1Context _context;
		public Delete_StudentModel(PRN211_1Context context)
		{
			_context = context;
		}
		public Student NewStudent { get; set; }
		public IActionResult OnGet(string id)
        {
			if (id == null)
			{
				return RedirectToPage("Index_Student");
			}
			else
			{
				int parseId;
				if (!int.TryParse(id, out parseId))
				{
					return RedirectToPage("Index_Student");
				}
				else
				{
					NewStudent = _context.Students.Find(int.Parse(id));
					if (NewStudent != null)
					{
						_context.Students.Remove(NewStudent);
						_context.SaveChanges();
					}
				}
			}
			return RedirectToPage("Index_Student");
		}
    }
}
