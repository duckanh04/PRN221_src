using LoadDB_EF_RazorPage.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Runtime.CompilerServices;

namespace LoadDB_EF_RazorPage.Pages.Students
{
    public class Add_StudentModel : PageModel
    {
        private readonly PRN211_1Context _context;
        [BindProperty]
        public Student NewStudent { get; set; }

        public List<Department> Departments { get; private set; }
        public Add_StudentModel(PRN211_1Context context)
        {
            _context = context;
        }
        public IActionResult OnGet()
        {
            //Departments = _context.Departments.ToList();
            ViewData["Departments"] = new SelectList(_context.Departments, "Id", "Name");
            return Page();
        }

        public IActionResult OnPost()
        {
            var x = _context.Students.Find(NewStudent.Id);
            if (!ModelState.IsValid)
            {
                Departments = _context.Departments.ToList();
                return Page();
            }
            if (x == null)
            {
                _context.Students.Add(NewStudent);
                _context.SaveChanges();
            }
            else { return Page(); }
            return RedirectToPage("Index_Student");
        }

        /*
         * cach 2 ( co toi 4 cach de thuc hien step nay)
         * int id = int.Parse(Request.Form["id"]);
         */
    }
}
