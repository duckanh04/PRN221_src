using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using LoadDB_EF_RazorPage.Models;
using Microsoft.EntityFrameworkCore;

namespace LoadDB_EF_RazorPage.Pages.Students
{
    public class Index_StudentModel : PageModel
    {
        private readonly PRN211_1Context _context;
        public Index_StudentModel(PRN211_1Context context) 
        {
            _context = context;
        }

        public List<Student> Stu { get; set; }

		public List<Department> Depts { get; set; }
		public void OnGet()
        {
            Stu = _context.Students.Include(x => x.Depart).ToList();
            Depts = _context.Departments.ToList();
        }
    }
}
