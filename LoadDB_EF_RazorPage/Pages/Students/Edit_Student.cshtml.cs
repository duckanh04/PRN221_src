using LoadDB_EF_RazorPage.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LoadDB_EF_RazorPage.Pages.Students
{
    public class Edit_StudentModel : PageModel
    {
        private readonly PRN211_1Context _context;
        public Edit_StudentModel(PRN211_1Context context)
        {
            _context = context;
        }
        [BindProperty]
        public Student NewStudent { get; set; }
        public void OnGet(string id)
        {
            if (id == null)
            {
				RedirectToPage("Index_Student");
			}
            else
            {
                int parseId;
                if (!int.TryParse(id, out parseId))
                {
					RedirectToPage("Index_Student");
				}
                else
                {
					NewStudent = _context.Students.Find(int.Parse(id));
                    if (NewStudent != null)
                    {
						ViewData["Departments"] = new SelectList(_context.Departments, "Id", "Name");
					}
				}
            }
		}

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var st = _context.Students.Find(NewStudent.Id);
                if (st != null)
                {
                    st.Name = NewStudent.Name;
                    st.Gender = NewStudent.Gender;
                    st.DepartId = NewStudent.DepartId;
                    st.Dob = NewStudent.Dob;
                    st.Gpa = NewStudent.Gpa;
                    _context.SaveChanges();
                }
            }
            return RedirectToPage("Index_Student");
        }
    }
}
