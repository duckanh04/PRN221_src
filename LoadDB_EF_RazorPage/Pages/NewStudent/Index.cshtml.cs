﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using LoadDB_EF_RazorPage.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LoadDB_EF_RazorPage.Pages.NewStudent
{
    public class IndexModel : PageModel
    {
        private readonly LoadDB_EF_RazorPage.Models.PRN211_1Context _context;

        public IndexModel(LoadDB_EF_RazorPage.Models.PRN211_1Context context)
        {
            _context = context;
        }

        public IList<Student> Student { get;set; } = default!;

        [BindProperty]
        public Student Std { get; set; } = default!;

        public List<string> list { get; set; } = new List<string>();

        public IList<Department> Depart { get; set; } = default!;
        public void OnGet(string? nameFilter)
        {
            //if (_context.Students != null)
            //{
            //    Student = _context.Students.Include(s => s.Depart).ToList();
            //    Depart = _context.Departments.ToList();
            //}
            //ViewData["DepartId"] = new SelectList(_context.Departments, "Id", "Name");
            ViewData["NameFilter"] = nameFilter;
            var studentsQuery = _context.Students.Include(x => x.Depart).AsQueryable();

            if (!string.IsNullOrEmpty(nameFilter))
            {
                studentsQuery = studentsQuery.Where(s => s.Name.Contains(nameFilter));
            }

            Student = studentsQuery.ToList();
            Depart = _context.Departments.ToList();
            ViewData["DepartId"] = new SelectList(_context.Departments, "Id", "Name");
        }

        public IActionResult OnPost(IFormCollection f, string? nameFilter)
        {
            list = new List<string>();
            if (f.ContainsKey("all"))
            {
                list.Add("all");
                Student = _context.Students.Include(s => s.Depart).ToList();
            }
            else
            {
                foreach (var d in _context.Departments.ToList())
                {
                    if (f.ContainsKey("chk" + d.Id)) list.Add(d.Id);
                }
                Student = _context.Students.Include(s => s.Depart).Where(x => list.Contains(x.DepartId)).ToList();
            }
            if (!string.IsNullOrEmpty(nameFilter))
            {
                Student = Student.Where(s => s.Name.Contains(nameFilter)).ToList();
            }
            Depart = _context.Departments.ToList();
            return Page();
        }
    }
}
