﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LoadDB_EF.Models;
using Microsoft.EntityFrameworkCore;

namespace LoadDB_EF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public PRN211_1Context context = new PRN211_1Context();
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void Load()
        {
            var de = PRN211_1Context.ins.Departments.Select(x => x.Name).ToList();
            var st = PRN211_1Context.ins.Students.Include(x => x.Depart).ToList();
            dgvDisplay.ItemsSource = st;
            cbxDept.ItemsSource = de;

            var data_search = de;
            data_search.Insert(0, string.Empty);
            search_cbxDepart.ItemsSource = data_search;
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            Student st = GetStudent();
            if (st != null)
            {
                //bool idExisted = context.Students.Any(x => x.Id == int.Parse(txtId.Text));
                var x = PRN211_1Context.ins.Students.Find(st.Id);
                if (x != null)
                {
                    MessageBox.Show("ID existed. Can not add!!!");
                }
                else
                {

                    context.Students.Add(st);
                    context.SaveChanges();
                    Load();
                    MessageBox.Show("Student added successfully!");
                }
            }
        }

        private Student GetStudent()
        {
            if (!int.TryParse(txtId.Text, out int id))
            {
                MessageBox.Show("Id must be an integer!!");
                return null;
            }

            if (!float.TryParse(txtGpa.Text, out float gpa))
            {
                MessageBox.Show("GPA must be a number!!");
                return null;
            }

            string Name = txtName.Text;
            bool Gender = (bool)chkGender.IsChecked;
            string DepartName = cbxDept.SelectedValue?.ToString();

            if (string.IsNullOrEmpty(DepartName))
            {
                MessageBox.Show("Please select a department!!");
                return null;
            }

            string DepartId = PRN211_1Context.ins.Departments.FirstOrDefault(x => x.Name == DepartName)?.Id;

            if (DepartId == null)
            {
                MessageBox.Show("Invalid department selected!!");
                return null;
            }

            DateTime Dob = dpkDob.SelectedDate ?? DateTime.MinValue;

            return new Student()
            {
                Id = id,
                Name = Name,
                Gender = Gender,
                DepartId = DepartId,
                Dob = Dob,
                Gpa = gpa
            };
        }

        private void dgvDisplay_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            Student st = (Student)dgvDisplay.SelectedValue;
            txtId.Text = st.Id.ToString();
            txtName.Text = st.Name;
            chkGender.IsChecked = st.Gender;
            cbxDept.SelectedValue = st.Depart.Name;
            dpkDob.SelectedDate = st.Dob;
            txtGpa.Text = st.Gpa.ToString();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Student st = GetStudent();
            if (st != null)
            {
                //bool idExisted = context.Students.Any(x => x.Id == int.Parse(txtId.Text));
                var newContext = new PRN211_1Context();
                var x = newContext.Students.Find(st.Id);
                if (x == null)
                {
                    MessageBox.Show("ID not existed. Can not update!!!");
                }
                else
                {
                    context.Students.Update(st);
                    context.SaveChanges();
                    MessageBox.Show("Student update successfully!");
                    Load();
                }
                Load();
            }
            Load();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            bool isMaleSelected = search_rdbMale.IsChecked ?? false;
            bool isFemaleSelected = search_rdbFemale.IsChecked ?? false;
            string selectedDepart = search_cbxDepart.SelectedValue as string;

            IEnumerable<Student> filteredStudents = PRN211_1Context.ins.Students.Include(x => x.Depart).ToList();

            if (isMaleSelected)
            {
                filteredStudents = filteredStudents.Where(student => student.Gender == true);
            }
            else if (isFemaleSelected)
            {
                filteredStudents = filteredStudents.Where(student => student.Gender == false);
            }

            if (!string.IsNullOrEmpty(selectedDepart))
            {
                filteredStudents = filteredStudents.Where(student => student.Depart.Name == selectedDepart);
            }

            dgvDisplay.ItemsSource = filteredStudents.ToList();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Student st = GetStudent();
            //bool idExisted = context.Students.Any(x => x.Id == int.Parse(txtId.Text));
            var x = PRN211_1Context.ins.Students.Find(st.Id);
            if (x == null)
            {
                MessageBox.Show("ID not existed. Can not delete!!!");
            }
            else
            {
                context.Students.Remove(st);
                context.SaveChanges();
                MessageBox.Show("Student update successfully!");
                Load();
            }
        }


    }
}
