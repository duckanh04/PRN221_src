﻿using System;
using System.Threading;
using System.Threading.Tasks;

/*class Program
{
    static void PrintNumber(string msg)
    {
        for(int i = 1; i <= 5; i++)
        {
            Console.WriteLine($"{msg}:{i}");
            Thread.Sleep(1000);
        }
    }
    static void Main()
    {
        Thread.CurrentThread.Name = "Main";
        Task task01 = new Task(() => PrintNumber("Task 01"));
        Task task02 = Task.Run(delegate
        {
            PrintNumber("Task 02");
        });
        Task task03 = new Task(new Action(() => { PrintNumber("Task 03");}));

    }
*/

/*class Program
{
    public static void Main()
    {
        Task[] tasks = new Task[5];
        string taskData = "Hello";
        for (int i =  0; i < 5; i++)
        {
            tasks[i] = Task.Run(() =>
            {
                Console.WriteLine($"Task = {Task.CurrentId}, obj = {taskData}, " +
                    $"ThreadId = {Thread.CurrentThread.ManagedThreadId}");
                Thread.Sleep(1000);
            });
        }
        try
        {
            Task.WaitAll(tasks);
        }
        catch (AggregateException ae)
        {
            Console.WriteLine("One or more exceptions occucured: ");
            foreach (var ex in ae.Flatten().InnerExceptions)
                Console.WriteLine(" {0}", ex.Message);
        }
        Console.WriteLine("Status of completed tasks: ");
        foreach (var t in tasks)
        {
            Console.WriteLine(" Task #{0} : {1}", t.Id, t.Status);
        }
    }
}*/

class Program
{
    private static double DoComputation(double start)
    {
        double sum = 0;
        for (var value = start; value <= start + 10; value += .1)
        {
            sum += value;
        }
        return sum;
    }

    public static void Main()
    {
        Task<Double>[] taskArray = {Task<double>.Factory.StartNew(() => DoComputation(1.0)),
        Task<double>.Factory.StartNew(() => DoComputation(100.0)),
        Task<double>.Factory.StartNew(() => DoComputation(1000.0))};
        var result = new double[taskArray.Length];
        double sum = 0;
        for (int i = 0; i < taskArray.Length; i++)
        {
            result[i] = taskArray[i].Result;
            Console.WriteLine("{0:N1} {1}", result[i],
                i == taskArray.Length - 1 ? "=" : "+");
            sum += result[i];
        }
        Console.WriteLine("{0:N1}", sum);
    }
}