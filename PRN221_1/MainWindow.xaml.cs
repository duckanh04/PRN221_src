﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PRN221
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            rdbMale.IsChecked = true;
            cmbMajor.SelectedIndex = 0;
            ckbVn.IsChecked = true;
            lblPass.Visibility = Visibility.Collapsed;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CheckPass();
        }

        private void ShowInfo()
        {
            string rsString = "Name: " + txtName.Text + "\n" + "Id: " + txtId.Text + "\n" + "Mail: " + txtMail.Text + "\n";

            string rsGender = "Gender: ";

            if (rdbFemale.IsChecked == true)
            {
                rsGender += "Female" + "\n";
            }
            else if (rdbMale.IsChecked == true)
            {
                rsGender += "Male" + "\n";
            }
            else if (rdbOther.IsChecked == true)
            {
                rsGender += "Others" + "\n";
            }

            string rsMajor = "Major: ";

            if (cmbMajor.SelectedIndex == 0)
            {
                rsMajor += ".NET" + "\n";
            }
            else if (cmbMajor.SelectedIndex == 1)
            {
                rsMajor += "Brse" + "\n";
            }
            else if (cmbMajor.SelectedIndex == 2)
            {
                rsMajor += "Web" + "\n";
            }

            string rsLan = "Language: ";

            if (ckbVn.IsChecked == true)
            {
                rsLan += "Vietnamese" + "\n";
            }
            if (ckbEn.IsChecked == true)
            {
                rsLan += "English" + "\n";
            }
            if (ckbCh.IsChecked == true)
            {
                rsLan += "Chinese" + "\n";
            }
            if (ckbJp.IsChecked == true)
            {
                rsLan += "Japanese" + "\n";
            }

            MessageBox.Show(rsString + rsGender + rsMajor + rsLan, "Student Infomation");
        }

        private void CheckPass()
        {
            if (txtPass.Password == "0411")
            {
                lblPass.Visibility = Visibility.Collapsed;
                ShowInfo();
            } else 
            {   
                lblPass.Visibility = Visibility.Visible;
                MessageBox.Show("Wrong password. \nEnter again!");
            }
        }
    }
}
