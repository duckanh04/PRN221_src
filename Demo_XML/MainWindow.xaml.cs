﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;
using Demo_XML.Models;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace Demo_XML
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        List<Person> person = new List<Person>();
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var save_dialog = new SaveFileDialog() { Filter = "XML Files (*.xml)|*.xml|All File(*.*)|*.*" };
            var result = save_dialog.ShowDialog();
            if (result == false) { return; }
            var xmls = new XmlSerializer(typeof(List<Person>));
            using Stream s1 = File.Create(save_dialog.FileName);
            xmls.Serialize(s1, person);
            s1.Close();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            var open_dialog = new OpenFileDialog() { Filter = "XML Files (*.xml)|*.xml|All File(*.*)|*.*" };
            var result = open_dialog.ShowDialog();
            if (result == false) { return; }
            var xmls = new XmlSerializer(typeof(List<Person>));
            using Stream s2 = File.OpenRead(open_dialog.FileName);
            person = (List<Person>)xmls.Deserialize(s2);
            dgvDisplay.ItemsSource = person;
            s2.Close();
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(txtId.Text);
            var x = person.FirstOrDefault(x => x.Id == id);
            if (x != null)
            {
                MessageBox.Show("Id existed, cannot add");
                return;
            }
            string name = txtName.Text;
            bool gender = (bool)chkGender.IsChecked;
            person.Add(new Person() { Id = id, Name = name, Gender = gender });
            dgvDisplay.ItemsSource = person;
            dgvDisplay.Items.Refresh();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(txtId.Text);
            var x = person.FirstOrDefault(x => x.Id == id);
            if (x == null) { return; }
            x.Name = txtName.Text;
            x.Gender = (bool)chkGender.IsChecked;
            dgvDisplay.ItemsSource = person;
            dgvDisplay.Items.Refresh();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(txtId.Text);
            for (int i = 0; i < person.Count; i++)
            {
                if (person[i].Id == id)
                {
                    person.RemoveAt(i);
                    i--;
                }
            }
            dgvDisplay.ItemsSource = person;
            dgvDisplay.Items.Refresh();
        }

        private void dgvDisplay_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {

        }

        private void btnLoadJson_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog() { Filter = "Json Files (*.json) |*.json|All File(*.*)|*.*" };
            var result = ofd.ShowDialog();
            if (result == null) { return; }
            string s2 = File.ReadAllText(ofd.FileName);
            person = System.Text.Json.JsonSerializer.Deserialize<List<Person>>(s2);
            dgvDisplay.ItemsSource = person;
        }

        private void btnSaveJson_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new SaveFileDialog() { Filter = "Json Files (*.json) |*.json|All File(*.*)|*.*" };
            var result = ofd.ShowDialog();
            if (result == null) { return; }
            var json = System.Text.Json.JsonSerializer.Serialize<List<Person>>(person);
            File.WriteAllText(ofd.FileName, json);
        }

        private void btnJsontoXml_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "JSON files (*.json)|*.json";
                if (openFileDialog.ShowDialog() == true)
                {
                    string jsonString = File.ReadAllText(openFileDialog.FileName);
                   
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(JsonConvert.DeserializeXmlNode(jsonString, "Root").OuterXml);

                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.Filter = "XML files (*.xml)|*.xml";
                    if (saveFileDialog.ShowDialog() == true)
                    {
                        xmlDocument.Save(saveFileDialog.FileName);
                        MessageBox.Show("Conversion successful! XML file saved.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnXmltoJson_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "XML files (*.xml)|*.xml";
                if (openFileDialog.ShowDialog() == true)
                {
                    string xmlString = File.ReadAllText(openFileDialog.FileName);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(xmlString);

                    string jsonString = JsonConvert.SerializeXmlNode(xmlDoc);

                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.Filter = "JSON files (*.json)|*.json";
                    if (saveFileDialog.ShowDialog() == true)
                    {
                        File.WriteAllText(saveFileDialog.FileName, jsonString);
                        MessageBox.Show("Conversion successful! JSON file saved.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
