﻿
class ChineseEngine : IEngine
{
    public void Start()
    {
        Console.WriteLine("ChineseEngine Starting");
    }
}

class AmericanEngine : IEngine
{
    public void Start()
    {
        Console.WriteLine("AmericanEngine Starting");
    }
}

class JapanEngine : IEngine
{
    public void Start()
    {
        Console.WriteLine("JapanEngine Starting");
    }
}

interface IEngine
{
    public void Start();
}

class Car
{
    public static void Move(IEngine e)
    {
        e.Start();
        Console.WriteLine("Car moving");
    }

    public static void Main()
    {
        IEngine e = new JapanEngine();
        Car c = new Car();
        Car.Move(e);
        Console.ReadLine();
    }
}