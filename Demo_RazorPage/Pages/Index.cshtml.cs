﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Globalization;

namespace Demo_RazorPage.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }
        public string Name { get; set; }
        public void OnGet()
        {
            //ViewData["name"] = "Duc Anh deptrai";

            Name = "duc anh hehe";
        }
    }
}