﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Project_PRN221_1.Models
{
    public partial class Project_PRN221_1Context : DbContext
    {
        public Project_PRN221_1Context()
        {
        }

        public Project_PRN221_1Context(DbContextOptions<Project_PRN221_1Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Comment> Comments { get; set; } = null!;
        public virtual DbSet<Post> Posts { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=DUCANHDEPTRAIVO\\SQLEXPRESS;database=Project_PRN221_1;uid=sa;pwd=123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.CategoryId).HasColumnName("categoryID");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(20)
                    .HasColumnName("categoryName")
                    .IsFixedLength();
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.Property(e => e.CommentId).HasColumnName("commentId");

                entity.Property(e => e.CommentContent)
                    .HasMaxLength(200)
                    .HasColumnName("commentContent")
                    .IsFixedLength();

                entity.Property(e => e.CommentDate)
                    .HasColumnType("datetime")
                    .HasColumnName("commentDate");

                entity.Property(e => e.PostId).HasColumnName("postId");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comments_Posts");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comments_Users");
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.PostId).HasColumnName("postId");

                entity.Property(e => e.CategoryId).HasColumnName("categoryId");

                entity.Property(e => e.Image1Path)
                    .HasMaxLength(100)
                    .HasColumnName("image_1_Path")
                    .IsFixedLength();

                entity.Property(e => e.Image2Path)
                    .HasMaxLength(100)
                    .HasColumnName("image_2_Path")
                    .IsFixedLength();

                entity.Property(e => e.Image3Path)
                    .HasMaxLength(100)
                    .HasColumnName("image_3_Path")
                    .IsFixedLength();

                entity.Property(e => e.Image4Path)
                    .HasMaxLength(100)
                    .HasColumnName("image_4_Path")
                    .IsFixedLength();

                entity.Property(e => e.Image5Path)
                    .HasMaxLength(100)
                    .HasColumnName("image_5_Path")
                    .IsFixedLength();

                entity.Property(e => e.PostDate)
                    .HasColumnType("datetime")
                    .HasColumnName("postDate");

                entity.Property(e => e.PostGraph1)
                    .HasMaxLength(100)
                    .HasColumnName("post_graph_1")
                    .IsFixedLength();

                entity.Property(e => e.PostGraph2)
                    .HasMaxLength(100)
                    .HasColumnName("post_graph_2")
                    .IsFixedLength();

                entity.Property(e => e.PostGraph3)
                    .HasMaxLength(100)
                    .HasColumnName("post_graph_3")
                    .IsFixedLength();

                entity.Property(e => e.PostGraph4)
                    .HasMaxLength(100)
                    .HasColumnName("post_graph_4")
                    .IsFixedLength();

                entity.Property(e => e.PostGraph5)
                    .HasMaxLength(100)
                    .HasColumnName("post_graph_5")
                    .IsFixedLength();

                entity.Property(e => e.PostHeader)
                    .HasMaxLength(100)
                    .HasColumnName("postHeader")
                    .IsFixedLength();

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.Property(e => e.Views).HasColumnName("views");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Posts_Categories");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Posts_Users");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.RoleId).HasColumnName("roleId");

                entity.Property(e => e.RoleName)
                    .HasMaxLength(20)
                    .HasColumnName("roleName")
                    .IsFixedLength();
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email")
                    .IsFixedLength();

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .HasColumnName("name")
                    .IsFixedLength();

                entity.Property(e => e.Password)
                    .HasMaxLength(16)
                    .HasColumnName("password")
                    .IsFixedLength();

                entity.Property(e => e.RoleId).HasColumnName("roleId");

                entity.Property(e => e.Username)
                    .HasMaxLength(20)
                    .HasColumnName("username")
                    .IsFixedLength();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Users_Roles");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
