﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public IndexModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        public List<Category> Categories { get; set; }

        public Post LastestPost { get; set; }

        public List<Post> PopularPost { get; set; }
        public void OnGet()
        {
            Categories = _context.Categories.ToList();
            LastestPost = _context.Posts.OrderByDescending(p => p.PostDate).FirstOrDefault();
            PopularPost = _context.Posts.OrderByDescending(popu => popu.Views).Take(10).ToList();
        }
    }
}